package ijsenese.bux.bot.api.model;

/**
 * {
 * "productId" : "sb26493",
 * "investingAmount" : {
 * "currency": "BUX",
 * "decimals": 2,
 * "amount": "10.00"
 * },
 * "leverage" : 2,
 * "direction" : "BUY",
 * "source": {
 * "sourceType": "OTHER"
 * }
 * }
 */
public class BuyOrder {
    private String productId;
    private Amount investingAmount;
    private int leverage;
    private Direction direction;
    private Source source;

    public static BuyOrder oneUnitBuyOrder(String productId) {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.productId = productId;
        buyOrder.direction = Direction.BUY;
        buyOrder.leverage = 2;
        buyOrder.source = Source.otherSourceType();
        buyOrder.investingAmount = Amount.standardBuyAmount();
        return buyOrder;
    }

    public BuyOrder() {
    }

    public String getProductId() {
        return productId;
    }

    public Amount getInvestingAmount() {
        return investingAmount;
    }

    public int getLeverage() {
        return leverage;
    }

    public Direction getDirection() {
        return direction;
    }

    public Source getSource() {
        return source;
    }
}

