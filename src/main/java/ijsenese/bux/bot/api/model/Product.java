package ijsenese.bux.bot.api.model;

public class Product {
    private String securityId;
    private String symbol;
    private String displayName;

    private Amount currentPrice;
    private Amount closingPrice;

    private long dateCreated;

    public Product() {
        //empty constructor
    }

    public String getSecurityId() {
        return securityId;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Amount getCurrentPrice() {
        return currentPrice;
    }

    public Amount getClosingPrice() {
        return closingPrice;
    }

    public long getDateCreated(){
        return dateCreated;
    }
}
