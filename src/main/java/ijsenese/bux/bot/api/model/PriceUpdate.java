package ijsenese.bux.bot.api.model;

import java.util.Map;

public class PriceUpdate {
    private String securityId;
    private double currentPrice;

    public static PriceUpdate fromMap(Map<String, String> map) {
        PriceUpdate priceUpdate = new PriceUpdate();
        priceUpdate.securityId = map.get("securityId");
        priceUpdate.currentPrice = Double.valueOf(map.get("currentPrice"));
        return priceUpdate;
    }


    public PriceUpdate() {
    }
    public PriceUpdate(String id, double price) {
        this.securityId = id;
        this.currentPrice = price;
    }

    public String getSecurityId() {
        return securityId;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    @Override
    public String toString() {
        return "PriceUpdate{" +
                "securityId='" + securityId + '\'' +
                ", currentPrice=" + currentPrice +
                '}';
    }
}
