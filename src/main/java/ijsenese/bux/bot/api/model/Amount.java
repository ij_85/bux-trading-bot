package ijsenese.bux.bot.api.model;

public class Amount {
    private String currency;
    private int decimals;
    private String amount;

    public static Amount standardBuyAmount() {
        Amount a = new Amount();
        a.currency = "BUX";
        a.decimals = 2;
        a.amount = "10.00";
        return a;
    }

    public Amount() {
        //empty constructor
    }

    public String getCurrency() {
        return currency;
    }

    public int getDecimals() {
        return decimals;
    }

    public String getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Amount{" +
                "currency='" + currency + '\'' +
                ", decimals=" + decimals +
                ", amount='" + amount + '\'' +
                '}';
    }
}
