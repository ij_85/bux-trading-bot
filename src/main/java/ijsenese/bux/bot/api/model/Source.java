package ijsenese.bux.bot.api.model;

public class Source {
    private SourceType sourceType;

    public static Source otherSourceType() {
        Source s = new Source();
        s.sourceType = SourceType.OTHER;
        return s;

    }
    public Source() {
    }

    public SourceType getSourceType() {
        return sourceType;
    }
}
