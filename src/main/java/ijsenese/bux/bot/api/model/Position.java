package ijsenese.bux.bot.api.model;

public class Position {
    private String id;
    private String positionId;
    private Product product;
    private Amount investingAmount;
    private Amount price;
    private Amount profitAndLoss;
    private int leverage;
    private Direction direction;
    private String type;
    private long dateCreated;

    public Position() {
    }

    public String getId() {
        return id;
    }

    public String getPositionId() {
        return positionId;
    }

    public Product getProduct() {
        return product;
    }

    public Amount getInvestingAmount() {
        return investingAmount;
    }

    public Amount getPrice() {
        return price;
    }

    public Amount getProfitAndLoss() {
        return profitAndLoss;
    }

    public int getLeverage() {
        return leverage;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getType() {
        return type;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    @Override
    public String toString() {
        return "Position{" +
                "id='" + id + '\'' +
                ", positionId='" + positionId + '\'' +
                ", product=" + product +
                ", investingAmount=" + investingAmount +
                ", price=" + price +
                ", profitAndLoss=" + profitAndLoss +
                ", leverage=" + leverage +
                ", direction=" + direction +
                ", type='" + type + '\'' +
                ", dateCreated=" + dateCreated +
                '}';
    }
}

