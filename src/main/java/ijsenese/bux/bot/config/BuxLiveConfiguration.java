package ijsenese.bux.bot.config;


public class BuxLiveConfiguration implements Configuration {

    private final int remoteHttpPort = 80;
    private final String remoteHttpHost = "api.beta.getbux.com";
    private final int remoteWebSocketPort = 80;
    private final String remoteWebSocketHost = "rtf.beta.getbux.com";

    private final String authorizationHeader = "Bearer " +
            "eyJhbGciOiJIUzI1NiJ9.eyJyZWZyZXNoYWJsZSI6ZmFsc2UsInN1YiI6ImJiMGNkYTJiLWE" +
            "xMGUtNGVkMy1hZDVhLTBmODJiNGMxNTJjNCIsImF1ZCI6ImJldGEuZ2V0YnV4LmNvbSIsInN" +
            "jcCI6WyJhcHA6bG9naW4iLCJydGY6bG9naW4iXSwiZXhwIjoxODIwODQ5Mjc5LCJpYXQiOjE" +
            "1MDU0ODkyNzksImp0aSI6ImI3MzlmYjgwLTM1NzUtNGIwMS04NzUxLTMzZDFhNGRjOGY5MiI" +
            "sImNpZCI6Ijg0NzM2MjI5MzkifQ.M5oANIi2nBtSfIfhyUMqJnex-JYg6Sm92KPYaUL9GKg";

    private final String acceptedLanguageHeader = "nl-NL,en;q=0.8";

    private final String webSocketEndpoint = "wss://rtf.beta.getbux.com/subscriptions/me";

    private final String closePositionBaseEndpoint =
            "https://api.beta.getbux.com/core/21/users/me/portfolio/positions/";

    private final String openPositionEndpoint =
            "https://api.beta.getbux.com/core/21/users/me/trades";

    public BuxLiveConfiguration() {
    }

    @Override
    public String getRemoteHttpHost() {
        return remoteHttpHost;
    }

    @Override
    public int getRemoteHttpPort() {
        return remoteHttpPort;
    }

    @Override
    public String getRemoteWebSocketHost() {
        return remoteWebSocketHost;
    }

    @Override
    public int getRemoteWebSocketPort() {
        return remoteWebSocketPort;
    }

    @Override
    public String getAuthorizationHeader() {
        return authorizationHeader;
    }

    @Override
    public String getAcceptedLanguageHeader() {
        return acceptedLanguageHeader;
    }

    @Override
    public String getWebSocketEndpoint() {
        return webSocketEndpoint;
    }

    @Override
    public String getClosePositionBaseEndpoint() {
        return closePositionBaseEndpoint;
    }

    @Override
    public String getOpenPositionEndpoint() {
        return openPositionEndpoint;
    }
}
