package ijsenese.bux.bot.config;

public class LocalEnvironmentConfiguration implements Configuration {

    public String acceptedLanguageHeader = "nl-NL,en;q=0.8";

    public String websocketEndpoint = "ws://127.0.0.1:8080/subscriptions/me";

    public String closePositionBaseEndpoint =
            "http://127.0.0.1:8080/core/21/users/me/portfolio/positions/";

    public String openPositionEndpoint =
            "http://127.0.0.1:8080/core/21/users/me/trades";

    @Override
    public String getRemoteHttpHost() {
        return "127.0.0.1";
    }

    @Override
    public int getRemoteHttpPort() {
        return 8080;
    }

    @Override
    public String getRemoteWebSocketHost() {
        return "127.0.0.1";
    }

    @Override
    public int getRemoteWebSocketPort() {
        return 8080;
    }

    @Override
    public String getAuthorizationHeader() {
        return "Bearer " +
                "eyJhbGciOiJIUzI1NiJ9.eyJyZWZyZXNoYWJsZSI6ZmFsc2UsInN1YiI6ImJiMGNkYTJiLWE" +
                "xMGUtNGVkMy1hZDVhLTBmODJiNGMxNTJjNCIsImF1ZCI6ImJldGEuZ2V0YnV4LmNvbSIsInN" +
                "jcCI6WyJhcHA6bG9naW4iLCJydGY6bG9naW4iXSwiZXhwIjoxODIwODQ5Mjc5LCJpYXQiOjE" +
                "1MDU0ODkyNzksImp0aSI6ImI3MzlmYjgwLTM1NzUtNGIwMS04NzUxLTMzZDFhNGRjOGY5MiI" +
                "sImNpZCI6Ijg0NzM2MjI5MzkifQ.M5oANIi2nBtSfIfhyUMqJnex-JYg6Sm92KPYaUL9GKg";
    }

    @Override
    public String getAcceptedLanguageHeader() {
        return acceptedLanguageHeader;
    }

    @Override
    public String getWebSocketEndpoint() {
        return websocketEndpoint;
    }

    @Override
    public String getClosePositionBaseEndpoint() {
        return closePositionBaseEndpoint;
    }

    @Override
    public String getOpenPositionEndpoint() {
        return openPositionEndpoint;
    }
}
