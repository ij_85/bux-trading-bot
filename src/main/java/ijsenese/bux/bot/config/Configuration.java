package ijsenese.bux.bot.config;

public interface Configuration {
    String getRemoteHttpHost();

    int getRemoteHttpPort();

    String getRemoteWebSocketHost();

    int getRemoteWebSocketPort();

    String getAuthorizationHeader();

    String getAcceptedLanguageHeader();

    String getWebSocketEndpoint();

    String getClosePositionBaseEndpoint();

    String getOpenPositionEndpoint();
}
