package ijsenese.bux.bot;


public class TradeInstructions {
    private final String productId;
    private final double buyPrice;
    private final double upperLimitPrice;
    private final double lowerLimitPrice;

    public TradeInstructions(String productId, double buyPrice, double upperLimitPrice, double lowerLimitPrice) {
        this.productId = productId;
        this.buyPrice = buyPrice;
        this.upperLimitPrice = upperLimitPrice;
        this.lowerLimitPrice = lowerLimitPrice;
    }

    public String getProductId() {

        return productId;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public double getUpperLimitPrice() {
        return upperLimitPrice;
    }

    public double getLowerLimitPrice() {
        return lowerLimitPrice;
    }

    @Override
    public String toString() {
        return "TradeInstructions{" +
                "productId='" + productId + '\'' +
                ", buyPrice=" + buyPrice +
                ", upperLimitPrice=" + upperLimitPrice +
                ", lowerLimitPrice=" + lowerLimitPrice +
                '}';
    }
}
