package ijsenese.bux.bot;

import ijsenese.bux.bot.api.model.Amount;
import ijsenese.bux.bot.client.BuxServiceClient;
import ijsenese.bux.bot.client.BuxServiceClientException;
import ijsenese.bux.bot.client.BuxServiceClientImpl;
import ijsenese.bux.bot.config.BuxLiveConfiguration;
import ijsenese.bux.bot.config.Configuration;
import ijsenese.bux.bot.config.LocalEnvironmentConfiguration;
import ijsenese.bux.bot.json.Serializer;
import ijsenese.bux.bot.trading.TradingManager;
import ijsenese.bux.bot.trading.TradingManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

public class TradingApplication {
    public static Logger logger = LoggerFactory.getLogger(TradingApplication.class);

    public static void main(String[] args) {
        if (args.length != 4 || invalidArgs(Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]))) {
            logger.error(
                    "Expecting 4 arguments: product-id, buy-price, upper-limit-sell-price, lower-limit-sell-price");
            logger.error(
                    "Expecting the following relationship: lower-limit-sell-price < buy-price < upper-limit-sell-price");
            System.exit(1);
        }

        final TradeInstructions tradeInstructions =
                new TradeInstructions(args[0], Double.valueOf(args[1]), Double.valueOf(args[2]), Double.valueOf(args[3]));

        final Configuration configuration = getConfiguration();

        final Serializer serializer = new Serializer();
        final BuxServiceClient buxServiceClient = new BuxServiceClientImpl(configuration, serializer);
        final TradingManager tradingManager = new TradingManagerImpl(buxServiceClient);

        try {
            Amount result = tradingManager.executeTrade(tradeInstructions).get();
            logger.info("We made a profit of " + result.getAmount());
        } catch (BuxServiceClientException | ExecutionException | InterruptedException e) {
            logger.error("Error trading with the given instructions: " + tradeInstructions);
            System.exit(2);
        } finally {
            logger.info("Shutting down buxServiceClient");
            buxServiceClient.close();
        }
    }

    private static boolean invalidArgs(double buyPrice, double upperLimitPrice, double lowerLimitPrice) {
        return !((lowerLimitPrice < buyPrice) && (buyPrice < upperLimitPrice));
    }

    private static Configuration getConfiguration() {
        Configuration configuration;
        if (shouldRunAgainstLive()) {
            configuration = new BuxLiveConfiguration();
        } else {
            configuration = new LocalEnvironmentConfiguration();
        }
        return configuration;
    }

    private static boolean shouldRunAgainstLive() {
        return System.getenv("ENVIRONMENT") != null && System.getenv("ENVIRONMENT").equals("live");
    }
}
