package ijsenese.bux.bot.trading;

import ijsenese.bux.bot.api.model.PriceUpdate;
import ijsenese.bux.bot.client.DeregisterHandler;

public interface TradeHandler {
    /**
     * Called when a PriceUpdate is received
     * @param priceUpdate
     */
    void onPriceChange(PriceUpdate priceUpdate);

    /**
     * Called when an error occured while attempting to receive PriceUpdate
     * @param t
     */
    void onError(Throwable t);

    /**
     * Called when the PriceUpdate channel has been closed.
     */
    void onClose();

    /**
     * Registration method for passing callbacks meant to be triggered when the trade is complete
     * @param deregisterHandler
     */
    void addTradeCompleteCallback(DeregisterHandler deregisterHandler);
}
