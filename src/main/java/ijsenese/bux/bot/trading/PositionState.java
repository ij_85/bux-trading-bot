package ijsenese.bux.bot.trading;

import ijsenese.bux.bot.api.model.Position;

class PositionState {
    private Position openPosition;
    private Position closePosition;

    PositionState() {
        //empty constructor
    }

    boolean isPositionOpen() {
        return openPosition != null;
    }

    boolean isPositionClosed() {
        return closePosition != null;
    }

    Position getOpenPosition() {
        return openPosition;
    }

    Position getClosePosition() {
        return closePosition;
    }

    public void setOpenPosition(Position position) {
        this.openPosition = position;
    }

    public void setClosePosition(Position closePosition) {
        this.closePosition = closePosition;
    }
}
