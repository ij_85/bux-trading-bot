package ijsenese.bux.bot.trading;

import ijsenese.bux.bot.TradeInstructions;
import ijsenese.bux.bot.api.model.Amount;
import ijsenese.bux.bot.api.model.Position;
import ijsenese.bux.bot.api.model.PriceUpdate;
import ijsenese.bux.bot.client.BuxServiceClient;
import ijsenese.bux.bot.client.BuxServiceClientException;
import ijsenese.bux.bot.client.DeregisterHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static ijsenese.bux.bot.api.model.BuyOrder.oneUnitBuyOrder;

class TradeHandlerImpl implements TradeHandler {
    private static final Logger logger = LoggerFactory.getLogger(TradeHandlerImpl.class);
    private final Consumer<Throwable> externalErrorHandler;
    private final TradeInstructions tradeInstructions;
    private final BuxServiceClient buxServiceClient;
    private final CompletableFuture<Amount> result;

    private PositionState positionState = new PositionState();

    private boolean openPositionRequestPending = false;
    private boolean closePositionRequestPending = false;

    private final List<DeregisterHandler> deregisterHandlers = new ArrayList<>();

    public TradeHandlerImpl(
            TradeInstructions tradeInstructions,
            BuxServiceClient buxServiceClient,
            CompletableFuture<Amount> result,
            Consumer<Throwable> externalErrorHandler
    ) {
        this.externalErrorHandler = externalErrorHandler;
        this.tradeInstructions = tradeInstructions;
        this.buxServiceClient = buxServiceClient;
        this.result = result;
    }

    @Override
    public synchronized void onPriceChange(PriceUpdate priceUpdate) {
        try {
            priceUpdateHandler(priceUpdate);
        } catch (BuxServiceClientException e) {
            this.externalErrorHandler.accept(e);
        }
    }

    @Override
    public synchronized void onError(Throwable t) {
        logger.error("Something went wrong handlng the trade for instructions: " + tradeInstructions);
        logger.error("Error was: " + t);
        this.result.completeExceptionally(t);
        this.externalErrorHandler.accept(t);
    }

    @Override
    public void onClose() {
        logger.info("Price update stream closed");
    }

    @Override
    public synchronized void addTradeCompleteCallback(DeregisterHandler deregisterHandler) {
        if (this.positionState.isPositionClosed()) {
            deregisterHandler.call();
        } else {
            deregisterHandlers.add(deregisterHandler);
        }
    }

    private void priceUpdateHandler(PriceUpdate priceUpdate) throws BuxServiceClientException {
        logger.debug("Got price update: " + priceUpdate);

        if (shouldBuy(priceUpdate) && !this.openPositionRequestPending) {
            logger.debug("Requesting buy after price update " + priceUpdate);

            this.openPositionRequestPending = true;
            buxServiceClient.openPosition(
                    oneUnitBuyOrder(tradeInstructions.getProductId()), this::onOpenPosition, this::onError);

        } else if (shouldSell(priceUpdate) && !this.closePositionRequestPending) {
            logger.debug("Requesting sell after price update " + priceUpdate);

            this.closePositionRequestPending = true;
            buxServiceClient.closePosition(positionState.getOpenPosition(), this::onClosePosition, this::onError);

        } else {
            if (this.openPositionRequestPending) {
                logger.debug("No action taken, open position request is still pending");
            } else if (this.closePositionRequestPending) {
                logger.debug("No action taken, close position request is still pending");
            } else {
                logger.debug("No action taken, trading conditions not met");
            }
        }
    }


    private boolean shouldBuy(PriceUpdate priceUpdate) {
        return !positionState.isPositionOpen()
                && priceUpdate.getCurrentPrice() <= tradeInstructions.getBuyPrice()
                && priceUpdate.getCurrentPrice() >= tradeInstructions.getLowerLimitPrice();
    }

    private boolean shouldSell(PriceUpdate priceUpdate) {
        return positionState.isPositionOpen() && !positionState.isPositionClosed() &&
                (priceUpdate.getCurrentPrice() >= tradeInstructions.getUpperLimitPrice() ||
                        priceUpdate.getCurrentPrice() <= tradeInstructions.getLowerLimitPrice());
    }

    private synchronized void onOpenPosition(Position position) {
        this.positionState.setOpenPosition(position);
        this.openPositionRequestPending = false;
        logger.info("Opened position: " + position);
    }

    private synchronized void onClosePosition(Position position) {
        this.positionState.setClosePosition(position);
        this.closePositionRequestPending = false;
        logger.info("Closed position: " + position);
        deregisterHandlers.forEach(handler -> handler.call());
        this.complete();
    }

    private synchronized void complete() {
        this.result.complete(positionState.getClosePosition().getProfitAndLoss());
    }

}
