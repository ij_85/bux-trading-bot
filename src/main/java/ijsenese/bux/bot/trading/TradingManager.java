package ijsenese.bux.bot.trading;

import ijsenese.bux.bot.TradeInstructions;
import ijsenese.bux.bot.api.model.Amount;
import ijsenese.bux.bot.client.BuxServiceClientException;

import java.util.concurrent.CompletableFuture;

public interface TradingManager {
    /**
     * Execute the trade with the given tradeInstructions
     * @param tradeInstructions
     * @return future of the trade result (profit/loss)
     * @throws BuxServiceClientException
     */
    CompletableFuture<Amount> executeTrade(TradeInstructions tradeInstructions) throws BuxServiceClientException;
}
