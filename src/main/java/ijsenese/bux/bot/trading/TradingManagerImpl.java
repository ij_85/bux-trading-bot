package ijsenese.bux.bot.trading;

import ijsenese.bux.bot.TradeInstructions;
import ijsenese.bux.bot.api.model.Amount;
import ijsenese.bux.bot.client.BuxServiceClient;
import ijsenese.bux.bot.client.BuxServiceClientException;
import ijsenese.bux.bot.client.DeregisterHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;


public class TradingManagerImpl implements TradingManager {
    private static final Logger logger = LoggerFactory.getLogger(TradingManagerImpl.class);

    private final BuxServiceClient buxServiceClient;

    public TradingManagerImpl(BuxServiceClient buxServiceClient) {
        this.buxServiceClient = buxServiceClient;
    }

    @Override
    public CompletableFuture<Amount> executeTrade(TradeInstructions tradeInstructions) throws BuxServiceClientException {

        CompletableFuture<Amount> result = new CompletableFuture<>();

        logger.info("Executing new trade: " + tradeInstructions);

        final TradeHandlerImpl tradeHandlerImpl = new TradeHandlerImpl(tradeInstructions, buxServiceClient, result, this::errorHandler);

        final DeregisterHandler deregisterHandler = this.buxServiceClient.registerPriceObserver(
                tradeInstructions.getProductId(),
                tradeHandlerImpl);

        tradeHandlerImpl.addTradeCompleteCallback(deregisterHandler);
        return result;
    }

    private void errorHandler(Throwable t) {
        logger.error("Error encountered in trading manager: " +t);
    }


}


