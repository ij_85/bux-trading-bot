package ijsenese.bux.bot.client;

public interface DeregisterHandler {
    /**
     * Callback meant for giving access to unsubscribe functionality of the bux protocol
     */
    void call();
}
