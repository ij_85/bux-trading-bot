package ijsenese.bux.bot.client;

import ijsenese.bux.bot.api.model.Position;
import ijsenese.bux.bot.api.model.BuyOrder;
import ijsenese.bux.bot.trading.TradeHandler;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

public interface BuxServiceClient {
    /**
     * Send a close position request and blocks until response is received.
     * @param position
     * @throws ExecutionException
     * @throws InterruptedException
     */
    void closePosition(Position position, Consumer<Position> closePositionCallback, Consumer<Throwable> errorHandler) throws BuxServiceClientException;

    /**
     * Sends an open position request for the given productId and blocks until response is received
     * @param buyOrder
     * @return Position
     * @throws ExecutionException
     * @throws InterruptedException
     * @throws IOException
     */
    void openPosition(BuyOrder buyOrder, Consumer<Position> openPositionCallback, Consumer<Throwable> errorHandler) throws BuxServiceClientException;

    /**
     * Causes a websocket connection to be established and resulting price updates will be submitted to the supplied price watcher
     * @param productId
     * @param tradeHandler
     * @return DeregisterHandler for performing proper deregistration of the price update streams
     */
    DeregisterHandler registerPriceObserver(String productId, TradeHandler tradeHandler) throws BuxServiceClientException;

    /**
     * Shutdown the http client and associated resources
     */
    void close();
}
