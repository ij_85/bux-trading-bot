package ijsenese.bux.bot.client.protocol;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuxServerMessage {
    private String t;
    private BuxServerMessageBody body = new BuxServerMessageBody();

    public BuxServerMessage() {
        //empty constructor
    }

    public String getT() {
        return t;
    }

    public BuxServerMessageBody getBody() {
        return body;
    }

    public boolean isConnectionSuccededMessage() {
        return this.t != null && this.t.equals(BuxServerMessageConstants.CONNECTION_SUCCEDED);
    }

    public boolean isConnectionFailedMessage() {
        return this.t != null && this.t.equals(BuxServerMessageConstants.CONNECTION_FAILED);
    }

    public boolean isTradingQuoteMessage() {
        return this.t != null && this.t.equals(BuxServerMessageConstants.TRADING_QUOTE);
    }

}

@JsonIgnoreProperties(ignoreUnknown = true)
class BuxServerMessageBody {
    //known properties
    private String developerMessage;
    private String errorCode;
    private String securityId;
    private String currentPrice;
    private Map<String, String> unknownFields = new HashMap<>();

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getSecurityId() {
        return securityId;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }
}

interface BuxServerMessageConstants {
    String CONNECTION_FAILED = "connect.failed";
    String CONNECTION_SUCCEDED = "connect.connected";
    String TRADING_QUOTE = "trading.quote";
}
