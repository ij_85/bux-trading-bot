package ijsenese.bux.bot.client.protocol;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import ijsenese.bux.bot.api.model.PriceUpdate;
import ijsenese.bux.bot.trading.TradeHandler;
import ijsenese.bux.bot.json.Serializer;
import io.netty.util.concurrent.Future;
import org.asynchttpclient.ws.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static ijsenese.bux.bot.client.protocol.BuxClientSubscriptionMessage.subscriptionMessageFor;
import static ijsenese.bux.bot.client.protocol.BuxClientSubscriptionMessage.unsubscriptionMessageFor;

public class BuxProtocolSubscriptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(BuxProtocolSubscriptionHandler.class);
    private final String productId;
    private final TradeHandler tradeHandler;
    private final Serializer serializer;

    private boolean connectionInitialized = false;
    private boolean connectionFailed = false;

    private WebSocket webSocket;
    private boolean socketConnetionOpened;

    public BuxProtocolSubscriptionHandler(
            String productId,
            TradeHandler tradeHandler,
            Serializer serializer
    ) {
        this.productId = productId;
        this.tradeHandler = tradeHandler;
        this.serializer = serializer;
    }

    public synchronized void handleMessage(String message) throws IOException {
        if (!socketConnetionOpened || connectionFailed) {
            throw new IllegalStateException("Can't handle a message, web socket connection not open or failed");
        }
        if (!connectionInitialized) {
            handleInitialization(message);
        } else {
            BuxServerMessage buxServerMessage;
            try {
                buxServerMessage = serializer.fromJson(message, BuxServerMessage.class);
                if (buxServerMessage != null && buxServerMessage.isTradingQuoteMessage()) {
                    PriceUpdate priceUpdate =
                            new PriceUpdate(
                                    buxServerMessage.getBody().getSecurityId(),
                                    Double.valueOf(buxServerMessage.getBody().getCurrentPrice()));

                    tradeHandler.onPriceChange(priceUpdate);
                } else {
                    logger.warn("Got unexpected message from server: " + message);
                }
            } catch (JsonMappingException e) {
                logger.warn("Got unexpected message from server: " + message);
            }
        }
    }

    private void handleInitialization(String message) throws IOException {
        BuxServerMessage buxServerMessage = serializer.fromJson(message, BuxServerMessage.class);
        if (buxServerMessage.isConnectionSuccededMessage()) {
            this.connectionInitialized = true;
            sendSubscriptionRequest();
        } else if (buxServerMessage.isConnectionFailedMessage()) {
            this.connectionFailed = true;
        }
    }

    private Future<Void> sendSubscriptionRequest() throws JsonProcessingException {
        final BuxClientSubscriptionMessage subscriptionRequest = subscriptionMessageFor(productId);
        final String payload = serializer.toJson(subscriptionRequest);
        return this.webSocket.sendTextFrame(payload);
    }

    public Future<Void> closeSubscription() {
        logger.debug("Unsubscribing from price stream");
        final BuxClientSubscriptionMessage unsubscriptionMessage = unsubscriptionMessageFor(productId);
        final String payload;
        try {
            payload = serializer.toJson(unsubscriptionMessage);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return this.webSocket.sendTextFrame(payload);
    }

    public synchronized void setWebSocket(WebSocket websocket) {
        this.socketConnetionOpened = true;
        this.webSocket = websocket;
    }
}
