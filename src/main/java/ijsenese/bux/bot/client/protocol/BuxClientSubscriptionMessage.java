package ijsenese.bux.bot.client.protocol;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class BuxClientSubscriptionMessage {
    private List<String> subscribeTo = new ArrayList<>();
    private List<String> unsubscribeFrom = new ArrayList<>();

    public static BuxClientSubscriptionMessage subscriptionMessageFor(String productId) {
        return new BuxClientSubscriptionMessage(
                asList("trading.product." + productId),
                asList()
        );
    }

    public static BuxClientSubscriptionMessage unsubscriptionMessageFor(String productId) {
        return new BuxClientSubscriptionMessage(
                asList(),
                asList("trading.product." + productId)
            );

    }

    public BuxClientSubscriptionMessage() {
        //empty constructor
    }

    public BuxClientSubscriptionMessage(List<String> subscribeTo, List<String> unsubscribeFrom) {
        this.subscribeTo = subscribeTo;
        this.unsubscribeFrom = unsubscribeFrom;
    }

    public List<String> getSubscribeTo() {
        return subscribeTo;
    }

    public List<String> getUnsubscribeFrom() {
        return unsubscribeFrom;
    }
}
