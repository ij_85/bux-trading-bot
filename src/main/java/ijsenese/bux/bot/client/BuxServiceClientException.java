package ijsenese.bux.bot.client;

public class BuxServiceClientException extends Exception {
    public BuxServiceClientException(String message, Throwable e) {
        super(message, e);
    }
}
