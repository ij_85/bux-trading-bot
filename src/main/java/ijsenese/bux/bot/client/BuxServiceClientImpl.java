package ijsenese.bux.bot.client;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import ijsenese.bux.bot.api.model.BuyOrder;
import ijsenese.bux.bot.api.model.Position;
import ijsenese.bux.bot.client.protocol.BuxProtocolSubscriptionHandler;
import ijsenese.bux.bot.config.Configuration;
import ijsenese.bux.bot.json.Serializer;
import ijsenese.bux.bot.trading.TradeHandler;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Response;
import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketListener;
import org.asynchttpclient.ws.WebSocketUpgradeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import static org.asynchttpclient.Dsl.asyncHttpClient;

public class BuxServiceClientImpl implements BuxServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(BuxServiceClient.class);
    private final Configuration configuration;

    private final AsyncHttpClient httpClient;

    private final Serializer serializer;

    public BuxServiceClientImpl(Configuration configuration, Serializer serializer) {
        this.serializer = serializer;
        this.configuration = configuration;
        httpClient = asyncHttpClient();
    }

    @Override
    public void closePosition(
            Position position,
            Consumer<Position> closePositionCallback,
            Consumer<Throwable> errorHandler
    ) throws BuxServiceClientException {
        final ListenableFuture<Response> closePositionListeableFuture = httpClient.prepareDelete(this.configuration.getClosePositionBaseEndpoint() + position.getPositionId())
                .addHeader(HttpHeaderNames.AUTHORIZATION, configuration.getAuthorizationHeader())
                .addHeader(HttpHeaderNames.ACCEPT_LANGUAGE, configuration.getAcceptedLanguageHeader())
                .addHeader(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
                .addHeader(HttpHeaderNames.ACCEPT, HttpHeaderValues.APPLICATION_JSON)
                .execute();
        closePositionListeableFuture.addListener(
                runPositionRequestCallback(closePositionListeableFuture, closePositionCallback, errorHandler)
                , null);
    }


    @Override
    public void openPosition(
            BuyOrder buyOrder,
            Consumer<Position> openPositionCallback,
            Consumer<Throwable> errorHandler
    ) throws BuxServiceClientException {
        try {
            final ListenableFuture<Response> openPositionListenableFuture = httpClient.preparePost(this.configuration.getOpenPositionEndpoint())
                    .addHeader(HttpHeaderNames.AUTHORIZATION, configuration.getAuthorizationHeader())
                    .addHeader(HttpHeaderNames.ACCEPT_LANGUAGE, configuration.getAcceptedLanguageHeader())
                    .addHeader(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
                    .addHeader(HttpHeaderNames.ACCEPT, HttpHeaderValues.APPLICATION_JSON)
                    .setBody(serializer.toJson(buyOrder))
                    .execute();
            openPositionListenableFuture.addListener(
                    runPositionRequestCallback(openPositionListenableFuture, openPositionCallback, errorHandler), null);
        } catch (JsonProcessingException e) {
            throw new BuxServiceClientException("Error opening position", e);
        }
    }

    private Runnable runPositionRequestCallback(
            ListenableFuture<Response> listenableFuture,
            Consumer<Position> positionCallback,
            Consumer<Throwable> errorHandler
    ) {
        return () -> {
            try {
                String responseBody = listenableFuture.get().getResponseBody();
                try {
                    Position position = serializer.fromJson(responseBody, Position.class);
                    positionCallback.accept(position);
                } catch (JsonMappingException e) {
                    logger.error("Got bad message from server, while expecting position: " + responseBody);
                    errorHandler.accept(e);
                }
            } catch (InterruptedException | ExecutionException | IOException e) {
                logger.error("Error handling handling position request callback: " + e);
                errorHandler.accept(e);
            }
        };
    }

    private final List<WebSocket> webSockets = new ArrayList<>();

    @Override
    public synchronized DeregisterHandler registerPriceObserver(
            String productId,
            TradeHandler tradeHandler
    ) {
        try {
            BuxProtocolSubscriptionHandler buxProtocolSubscriptionHandler =
                    new BuxProtocolSubscriptionHandler(productId, tradeHandler, serializer);

            WebSocket singleWebSocket = httpClient.prepareGet(this.configuration.getWebSocketEndpoint())
                    .addHeader(HttpHeaderNames.AUTHORIZATION, configuration.getAuthorizationHeader())
                    .addHeader(HttpHeaderNames.ACCEPT_LANGUAGE, configuration.getAcceptedLanguageHeader())
                    .addHeader(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
                    .addHeader(HttpHeaderNames.ACCEPT, HttpHeaderValues.APPLICATION_JSON)
                    .execute(
                            new WebSocketUpgradeHandler.Builder()
                                    .addWebSocketListener(
                                            buildWebSocketHandler(tradeHandler, buxProtocolSubscriptionHandler))
                                    .build())
                    .toCompletableFuture().get();//blocking until websocket is setup

            webSockets.add(singleWebSocket);

            return buxProtocolSubscriptionHandler::closeSubscription;

        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Error trying to setup websocket for price update stream", e);
        }
    }

    private WebSocketListener buildWebSocketHandler(
            TradeHandler tradeHandler,
            BuxProtocolSubscriptionHandler buxProtocolSubscriptionHandler
    ) {
        return new WebSocketListener() {
            private BuxProtocolSubscriptionHandler subscriptionHandler = buxProtocolSubscriptionHandler;

            @Override
            public void onOpen(WebSocket websocket) {
                subscriptionHandler.setWebSocket(websocket);
            }

            @Override
            public void onClose(WebSocket websocket, int code, String reason) {
                tradeHandler.onClose();
            }

            @Override
            public void onError(Throwable t) {
                logger.error("WebSocket crashed");
                tradeHandler.onError(t);
            }

            @Override
            public void onTextFrame(String payload, boolean finalFragment, int rsv) {
                try {
                    subscriptionHandler.handleMessage(payload);
                } catch (IOException e) {
                    tradeHandler.onError(e);
                }
            }
        };
    }

    @Override
    public synchronized void close() {
        try {
            webSockets.forEach(ws -> ws.sendCloseFrame());
            this.httpClient.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
