package ijsenese.bux.bot.trading;

import ijsenese.bux.bot.TradeInstructions;
import ijsenese.bux.bot.api.model.BuyOrder;
import ijsenese.bux.bot.api.model.Direction;
import ijsenese.bux.bot.api.model.Position;
import ijsenese.bux.bot.api.model.PriceUpdate;
import ijsenese.bux.bot.client.BuxServiceClient;
import ijsenese.bux.bot.client.BuxServiceClientException;
import ijsenese.bux.bot.client.DeregisterHandler;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static se.mockachino.Mockachino.mock;
import static se.mockachino.Mockachino.verifyOnce;
import static se.mockachino.Mockachino.when;

public class TradingManagerImplTest {

    BuxServiceClientForTesting buxServiceClientForTesting = new BuxServiceClientForTesting();

    TradingManager tradingManager;
    @Before
    public void setUp() throws Exception {
        tradingManager = new TradingManagerImpl(buxServiceClientForTesting);
    }

    @Test
    public void tradeSellWhenUpperLimitIsReached() throws BuxServiceClientException {
        TradeInstructions t = tradingInstruction("id", 5, 10, 3);
        tradingManager.executeTrade(t);
        assertEquals(1, buxServiceClientForTesting.registerPriceObserverCalls.size());
        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        TradeHandler tradeHandler = buxServiceClientForTesting.registerPriceObserverCalls.get(0);

        tradeHandler.onPriceChange(priceUpdate("id", 6));

        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        tradeHandler.onPriceChange(priceUpdate("id", 4));

        assertEquals(1, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        BuyOrder openBuyOrder = buxServiceClientForTesting.openBuyOrderCalls.get(0);
        assertEquals(openBuyOrder.getDirection(), Direction.BUY);
        assertEquals(openBuyOrder.getInvestingAmount().getAmount(), "10.00");
        assertEquals(openBuyOrder.getInvestingAmount().getCurrency(), "BUX");
        assertEquals(openBuyOrder.getInvestingAmount().getDecimals(), 2);

        tradeHandler.onPriceChange(priceUpdate("id", 5));
        assertEquals(1, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        tradeHandler.onPriceChange(priceUpdate("id", 11));
        assertEquals(1, buxServiceClientForTesting.closePositionCalls.size());

        verifyOnce().on(buxServiceClientForTesting.deregisterHandler).call();
    }

    @Test
    public void tradeSellWhenLowerLimitIsReached() throws BuxServiceClientException {
        TradeInstructions t = tradingInstruction("id", 5, 10, 3);
        tradingManager.executeTrade(t);
        assertEquals(1, buxServiceClientForTesting.registerPriceObserverCalls.size());
        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        TradeHandler tradeHandler = buxServiceClientForTesting.registerPriceObserverCalls.get(0);

        tradeHandler.onPriceChange(priceUpdate("id", 6));

        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        tradeHandler.onPriceChange(priceUpdate("id", 4));

        assertEquals(1, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        BuyOrder openBuyOrder = buxServiceClientForTesting.openBuyOrderCalls.get(0);
        assertEquals(openBuyOrder.getDirection(), Direction.BUY);
        assertEquals(openBuyOrder.getInvestingAmount().getAmount(), "10.00");
        assertEquals(openBuyOrder.getInvestingAmount().getCurrency(), "BUX");
        assertEquals(openBuyOrder.getInvestingAmount().getDecimals(), 2);

        tradeHandler.onPriceChange(priceUpdate("id", 5));
        assertEquals(1, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        tradeHandler.onPriceChange(priceUpdate("id", 2));
        assertEquals(1, buxServiceClientForTesting.closePositionCalls.size());

        verifyOnce().on(buxServiceClientForTesting.deregisterHandler).call();
    }

    @Test
    public void noTradeSellWhenPriceLowerThanLowerLimit() throws BuxServiceClientException {
        TradeInstructions t = tradingInstruction("id", 5, 10, 3);
        tradingManager.executeTrade(t);
        assertEquals(1, buxServiceClientForTesting.registerPriceObserverCalls.size());
        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        TradeHandler tradeHandler = buxServiceClientForTesting.registerPriceObserverCalls.get(0);

        tradeHandler.onPriceChange(priceUpdate("id", 1));

        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());

        tradeHandler.onPriceChange(priceUpdate("id", 2));

        assertEquals(0, buxServiceClientForTesting.openBuyOrderCalls.size());
        assertEquals(0, buxServiceClientForTesting.closePositionCalls.size());
    }

    private TradeInstructions tradingInstruction(String id, double price, double upperLimit, double lowerLimit) {
        return new TradeInstructions(id, price, upperLimit, lowerLimit);
    }

    private PriceUpdate priceUpdate(String id, double currentPrice) {
        return new PriceUpdate(id, currentPrice);
    }

}

class BuxServiceClientForTesting implements BuxServiceClient {
    public List<TradeHandler> registerPriceObserverCalls = new ArrayList<>();
    public List<Position> closePositionCalls = new ArrayList<>();
    public List<BuyOrder> openBuyOrderCalls = new ArrayList<>();

    public DeregisterHandler deregisterHandler = mock(DeregisterHandler.class);

    @Override
    public void closePosition(
            Position position, Consumer<Position> closePositionCallback, Consumer<Throwable> errorHandler
    ) throws BuxServiceClientException {
        this.closePositionCalls.add(position);
        closePositionCallback.accept(position);
    }

    @Override
    public void openPosition(
            BuyOrder buyOrder, Consumer<Position> openPositionCallback, Consumer<Throwable> errorHandler
    ) throws BuxServiceClientException {
        this.openBuyOrderCalls.add(buyOrder);
        Position openPosition = mock(Position.class);
        when(openPosition.getId()).thenReturn("id");
        when(openPosition.getPrice()).thenReturn(buyOrder.getInvestingAmount());
        openPositionCallback.accept(openPosition);
    }

    @Override
    public DeregisterHandler registerPriceObserver(String productId, TradeHandler tradeHandler) throws BuxServiceClientException {
        registerPriceObserverCalls.add(tradeHandler);
        return deregisterHandler;
    }

    @Override
    public void close() {

    }
}