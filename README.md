#Bux trading bot
## Environment 
The project was developed using java8, namely jdk version: 1.8.0_171

## How to run
Use the following command for running against the test server:

```sh ./buildAndRunLocal.sh sb26493 12160 12180 11800```

Use the following command for running against the test server:

```sh ./buildAndRunBeta.sh sb26493 12160 12180 11800```

## Trading logic
Concerning the trading logic, a buy is made every time the price is lower or equal to the configured buy price AND the lowerLimit is not higher then the current price. If this would be the case then the bot would most likely sell immediately at a loss.


## Comments
I'd like to claim a lot of the limitations I'm mentioning here are just because I have to time box a coding challenge like this. In general I like to write a part where I explain issues I'm aware of:

* the project use the netty event loop threads to actually run the trading logic, in general it's a nono to use netty threads for anything but io.
* missing dependency injection framework like guice
* config is hardcode in config classes, this can obviously be improved with config files
* give the above, I'd also probably want to introduce some generic lifecycle managemnt utiliy classes rather than starting and stopping things like http clients manually
* no real focus has been given to performance aspects of the code, the choice of a netty based bot, was simply for the ease of modelling the logic through callbacks
* i might be ignoring something concerning decimal parts, given that they are mentioned in the api responses
* most importantly using double or in general floating point types for representing money is a big nono, because of representation issues
* if we configure the netty event loop thread pool to have one thread, we can remove all the synchronization from the code
* exception handling is a bit messy and hard to follow




