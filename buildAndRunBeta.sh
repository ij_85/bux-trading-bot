#!/usr/bin/env bash
./gradlew clean
./gradlew fatJar
env ENVIRONMENT=live java -jar ./build/libs/bux-bot-all.jar $@